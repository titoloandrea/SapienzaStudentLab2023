# Sapienza Laboratorio Archeologia del Paesaggio e GIS

Documentazione relativa al laboratorio "Il Paesaggio archeologico dell'Asia Occidentale Antica. Studio e analisi dei sistemi di indagine remote sensing, morfologici e topografici"

Questo repository contiene la documentazione relativa al sopracitato laboratorio.
In particolare, sono (o saranno) presenti i seguenti documenti:

- Sinossi del corso

- Slides (in html) di presentazione delle parti teoriche e dei passaggi delle parti pratiche

- Datasets per esercitazioni pratiche

**Note:** the slides were uploaded without embedded resources to avoid a larger repo size, thus downloading only the .html files will not work. In order for the slides to display correctly, the entire [slides](https://codeberg.org/titoloandrea/SapienzaStudentLab2023/src/branch/main/slides) folder needs to be downloaded.

#### Ispirazioni e fonti per teoria e dati

La parte pratica, soprattutto quella relativa a QGIS, ha preso ispirazione ed è in parte basata su diverse fonti che meritano una menzione:

- [Una breve introduzione al GIS](https://docs.qgis.org/3.16/it/docs/gentle_gis_introduction/index.html)
- [Manuale utente di QGIS 3.16](https://docs.qgis.org/3.16/it/docs/user_manual/index.html)
- [QGIS Tutorials by Ujaval Gandhi](https://www.qgistutorials.com/it/docs/introduction.html)
- [QGIS Introduction Tutorials by Evelyn Uuemaa](https://kevelyn1.github.io/QGIS-Intro/)
- [AnthroYeti QGSI4Arch Youtube Tutorials](https://invidious.namazso.eu/playlist?list=PLqiB3IIUNAnU8vPcuea6A9pB7Y_qQBH1u)

Gran parte dei dati utilizzati nel laboratorio provengono da fonti accessibili liberamente e gratuite. Alcuni dati sono il risultato di ricerche personali e non sono (ancora) disponibili online. 

Elenco di repository da cui si sono scaricati dati (continuamente aggiornati):

- [NaturalEarthData](https://www.naturalearthdata.com/)
- [Ancient World Mapping Center](https://awmc.unc.edu/wordpress/)
- [Geographic Data for Ancient Near Eastern Archaeological Sites](https://www.lingfil.uu.se/research/assyriology/earth/)
- [USGS Earth Explorer](https://earthexplorer.usgs.gov/)
- [CORONA Atlas & Referencing Systems](https://corona.cast.uark.edu/)

#### Licenza

Molte delle risorse a cui la parte pratica su QGIS si è ispirata sono state condivise sotto la licenza [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Per questo motivo, anche le risorse qui disponibili vengono condivise sotto lo stesso tipo di licenza. 
Si è quindi liberi di:

- Condividere: copiare e ridistribuire il materiale in qualsiasi supporto o formato. 
- Adattare: modificare, trasformare e rielaborare sul materiale stesso.

Basta solo dare un credito appropriato all'autore per l'opera originale e condividere eventuali materiali sotto la stessa licenza.

##### Struttura del repository (TBA):
